package standalone

import (
	"math/rand"

	"gitlab.com/akita/akita"
)

type TrafficInjector interface {
	RegisterAgent(a *Agent)
	InjectTraffic()
}

// GreedyTraffic generate a large number of traffic at the beginning of the
// simulation.
type GreedyTrafficInjector struct {
	agents []*Agent

	engine akita.Engine

	PacketSize int
	NumPackets int
}

func NewGreedyTrafficInjector(engine akita.Engine) *GreedyTrafficInjector {
	ti := new(GreedyTrafficInjector)
	ti.PacketSize = 1024
	ti.NumPackets = 1024
	ti.engine = engine
	return ti
}

func (ti *GreedyTrafficInjector) RegisterAgent(a *Agent) {
	ti.agents = append(ti.agents, a)
}

func (ti *GreedyTrafficInjector) InjectTraffic() {
	for i, a := range ti.agents {
		for j := 0; j < ti.NumPackets; j++ {
			dstID := rand.Int() % (len(ti.agents) - 1)
			if dstID >= i {
				dstID += 1
			}
			dst := ti.agents[dstID]

			pkt := NewStartSendEvent(0, a, dst, ti.PacketSize, j)
			ti.engine.Schedule(pkt)
		}
	}
}
