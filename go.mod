module gitlab.com/akita/noc

require (
	github.com/golang/mock v1.2.0
	github.com/onsi/ginkgo v1.7.0
	github.com/onsi/gomega v1.4.3
	gitlab.com/akita/akita v1.3.2
)
